import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue'),
  },
  {
    path: '/quiz',
    name: 'quiz',
    component: () => import(/* webpackChunkName: "quiz" */ '../views/Quiz.vue'),
  },
  {
    path: '/thank',
    name: 'thank',
    component: () => import(/* webpackChunkName: "thank" */ '../views/Thankyou.vue'),
  },
  {
    path: '/project-preview',
    name: 'project-preview',
    component: () => import(/* webpackChunkName: "project-preview" */ '../views/ProjectPreview.vue'),
  },
  {
    path: '*',
    name: 'not-found-page',
    component: () => import(/* webpackChunkName: "not-found-page" */ '../views/404.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
