import Vue from 'vue';
import VueGTM from 'vue-gtm';
import VueYandexMetrika from 'vue-yandex-metrika';
import config from './configs/analytics.config';

const VueInputMask = require('vue-inputmask').default;

Vue.use(VueInputMask);
Vue.use(VueGTM, config.gtm);
Vue.use(VueYandexMetrika, config.ym);

export default {
  VueInputMask,
  VueGTM,
  VueYandexMetrika,
};
