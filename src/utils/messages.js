export default {
  errors: {
    formFieldEmpty: {
      name: 'Укажите ваше имя',
      phone: 'Укажите ваш телефон',
      email: 'Укажите вашу эл. почту',
    },
    formFieldIncorrect: {
      name: 'Укажите ваше имя',
      phone: 'Введите корректный номер телефона',
      email: 'Введите корректный адрес эл. почты',
    },
  },
};
