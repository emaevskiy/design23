/* eslint-disable import/prefer-default-export */

function escapeHtmlChars(text) {
  const htmlCharsMap = {
    '&nbsp;': ' ',
    '&mdash;': '—',
    '&ndash': '–',
    '&hellip;': '…',
    '&sect;': '§',
    '&para;': '¶',
    '&laquo;': '«',
    '&raquo;': '»',
    '&lsaquo;': '‹',
    '&rsaquo;': '›',
    '&quot;': '"',
    '&lsquo;': '‘',
    '&rsquo;': '’',
    '&ldquo;': '“',
    '&rdquo;': '”',
    '&sbquo;': '‚',
    '&bdquo;': '„',
    '&bull;': '•',
  };

  return text.replace(/&(.*?);/g, (char) => {
    if (htmlCharsMap[char] !== undefined) {
      return htmlCharsMap[char];
    }
    return char;
  });
}

export { escapeHtmlChars };
