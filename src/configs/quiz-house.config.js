export default [
  {
    id: 1,
    type: 'question', // тип шага, может быть 'question' / 'result' / 'callback'
    question: 'Дизайн чего именно вам нужен?',
    // группировка ответов на вопросы в раздельные списки, свойство является обязателеным для шагов с типом question / result.
    answersGroups: [
      {
        title: 'Всего дома',
        template: 'text-list', // тип шаблона, может быть 'text-list' / 'cards'
        answers: [
          {
            value: '1 этаж',
            isActive: false,
          },
          {
            value: '2 этажа',
            isActive: false,
          },
          {
            value: '3 этажа и более',
            isActive: false,
          },
        ],
        isActive: true,
      },
      {
        title: 'Отдельные элементы',
        template: 'text-list',
        answers: [
          {
            value: 'Зал/Гостинная',
            isActive: false,
          },
          {
            value: 'Комната',
            isActive: false,
          },
          {
            value: 'Спальня',
            isActive: false,
          },
          {
            value: 'Кухня',
            isActive: false,
          },
          {
            value: 'Ванная',
            isActive: false,
          },
          {
            value: 'Детская комната',
            isActive: false,
          },
          {
            value: 'Другое',
            isActive: false,
          },
        ],
        isActive: true,
      },
    ],
    ymGoal: 'step_house_1',
    isCompleted: false,
  },
  {
    id: 2,
    type: 'question',
    question: 'Какая площадь вашего помещения?',
    answersGroups: [
      {
        title: null,
        template: 'text-list',
        answers: [
          {
            value: 'до 100 кв.м',
            trigger: {
              objectSquare: 70, // влияет на расчет стоимости проекта
            },
            isActive: false,
          },
          {
            value: 'от 100 до 200 кв.м',
            trigger: {
              objectSquare: 101,
            },
            isActive: false,
          },
          {
            value: 'свыше 200 кв.м',
            trigger: {
              objectSquare: 201,
            },
            isActive: false,
          },
        ],
        isActive: true,
      },
    ],
    ymGoal: 'step_house_2',
    isCompleted: false,
  },
  {
    id: 3,
    type: 'question',
    question: 'В каком стиле хотите проект?',
    answersGroups: [
      {
        title: null,
        template: 'text-list',
        answers: [
          {
            // image: '/assets/images/designs/classic.png',
            value: 'Классический',
            isActive: false,
          },
          {
            // image: '/assets/images/designs/loft.png',
            value: 'Лофт',
            isActive: false,
          },
          {
            // image: '/assets/images/designs/provence.png',
            value: 'Прованс',
            isActive: false,
          },
          {
            // image: '/assets/images/designs/high-tech.png',
            value: 'Хай-тек',
            isActive: false,
          },
          {
            // image: '/assets/images/designs/scandinavian.png',
            value: 'Скандинавский',
            isActive: false,
          },
          {
            // image: '/assets/images/designs/neoclassic.png',
            value: 'Неоклассика',
            isActive: false,
          },
          {
            // image: '/assets/images/designs/minimalistic.png',
            value: 'Минимализм',
            isActive: false,
          },
          {
            // image: '/assets/images/designs/others.png',
            value: 'Еще не определились',
            isActive: false,
          },
        ],
        isActive: true,
      },
    ],
    ymGoal: 'step_house_3',
    isCompleted: false,
  },
  {
    type: 'result',
    question: 'Когда планируете приступить к реализации?',
    answersGroups: [
      {
        title: null,
        answers: [
          {
            value: 'Как можно скорее. Требуется выезд специалиста на объект',
            trigger: {
              requestType: 'meet',
            }, // якорь для привязки какого-либо условия
            isActive: false,
          },
          {
            value: 'В ближайшее время. Требуется консультация по телефону',
            trigger: {
              requestType: 'call',
            },
            isActive: false,
          },
          {
            value: 'Просто интересуюсь. Хочу посмотреть чертежи и 3D',
            trigger: {
              requestType: 'none',
            },
            isActive: false,
          },
        ],
        isActive: true,
      },
    ],
    ymGoal: 'step_house_4',
    isCompleted: false,
  },
];
