/* eslint-disable max-len */
import router from '@/router';

const config = {
  gtm: {
    id: 'GTM-W6FBK3X', // Your GTM single container ID or array of container ids ['GTM-xxxxxxx', 'GTM-yyyyyyy']
    enabled: true, // defaults to true. Plugin can be disabled by setting this to false for Ex: enabled: !!GDPR_Cookie (optional)
    debug: true, // Whether or not display console logs debugs (optional)
    loadScript: true, // Whether or not to load the GTM Script (Helpful if you are including GTM manually, but need the dataLayer functionality in your components) (optional)
    vueRouter: router, // Pass the router instance to automatically sync with router (optional)
    ignoredViews: ['home'], // If router, you can exclude some routes name (case insensitive) (optional)}, // Google Tag Manager
  }, // Google Tag Manager
  ym: {
    id: 56286073,
    router,
    env: process.env.NODE_ENV,
    debug: true,
  }, // Yandex Metrika
};

// analytic configs
export default config;
