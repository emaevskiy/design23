import stepsApartaments from '@/configs/quiz-apartaments.config';
import stepsHouse from '@/configs/quiz-house.config';

export default {
  entry: {
    id: 0,
    type: 'question',
    question: 'Выберите вид вашего помещения:',
    answersGroups: [
      {
        title: null,
        answers: [
          {
            value: 'Квартира',
            trigger: {
              quizType: 'apartaments',
            },
            isActive: false,
          },
          {
            value: 'Частный дом',
            trigger: {
              quizType: 'house',
            },
            isActive: false,
          },
        ],
        isActive: true,
      },
    ],
    ymGoal: 'enter',
    isComplete: false,
  },
  default: [],
  apartaments: stepsApartaments,
  house: stepsHouse,
  // result: {
  //   type: 'result',
  //   question: 'Когда планируете приступить к реализации?',
  //   answersGroups: [
  //     {
  //       title: null,
  //       answers: [
  //         {
  //           value: 'В ближайшее время. Готовы обсудить детали',
  //           trigger: {
  //             callRequest: true,
  //           }, // якорь для привязки какого-либо условия
  //           isActive: false,
  //         },
  //         {
  //           value: 'Просто изучаем. Хотим посмотреть пример чертежей и 3D',
  //           trigger: {
  //             callRequest: false,
  //           },
  //           isActive: false,
  //         },
  //       ],
  //       isActive: true,
  //     },
  //   ],
  //   ymGoal: 'step1_4',
  //   isCompleted: false,
  // },
  callback: {
    type: 'callback',
  },
};
