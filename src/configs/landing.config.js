export default {
  designStyles: [
    {
      image: '/assets/images/designs/classic.png',
      title: 'Дизайн в классическом стиле',
    },
    {
      image: '/assets/images/designs/loft.png',
      title: 'Дизайн в стиле лофт',
    },
    {
      image: '/assets/images/designs/provence.png',
      title: 'Дизайн в стиле прованс',
    },
    {
      image: '/assets/images/designs/high-tech.png',
      title: 'Дизайн в стиле хай-тек',
    },
    {
      image: '/assets/images/designs/scandinavian.png',
      title: 'Дизайн в скандинавском стиле',
    },
    {
      image: '/assets/images/designs/neoclassic.png',
      title: 'Дизайн в стиле неоклассика',
    },
    {
      image: '/assets/images/designs/minimalistic.png',
      title: 'Дизайн в стиле минимализм',
    },
    {
      image: '/assets/images/designs/others.png',
      title: 'И еще более 50 стилей дизайна',
    },
  ],
  designInclusions: [
    {
      image: '/assets/images/inclusions/image_1.png',
      title: 'План расстановки мебели и сантехнического оборудованияе',
    },
    {
      image: '/assets/images/inclusions/image_2.png',
      title: 'План полотков с расстановкой освещения',
    },
    {
      image: '/assets/images/inclusions/image_3.png',
      title: 'План полов с указанием типа напольного покрытия',
    },
    {
      image: '/assets/images/inclusions/image_4.png',
      title: 'План демонтажа стен и перегородок',
    },
    {
      image: '/assets/images/inclusions/image_5.png',
      title: 'План размещения розеток, электровыводов  и выводов слаботочных сетей',
    },
    {
      image: '/assets/images/inclusions/image_6.png',
      title: 'Схема расположения кухонной и другой мебели',
    },
    {
      image: '/assets/images/inclusions/image_7.png',
      title: 'Ведомости отделочных материалов',
    },
    {
      image: '/assets/images/inclusions/image_8.png',
      title: '3D-визуализация дизайн-проекта помещения',
    },
  ],
  services: [
    {
      title: 'Перепланировка',
      image: '/assets/images/services/replaning.png',
      serviceIncludes: [
        'Планировка',
        'Рабочие чертежи для строителей',
      ],
    },
    {
      title: 'Базовый дизайн-проект',
      image: '/assets/images/services/base-project.png',
      serviceIncludes: [
        'Планировка',
        'Рабочие чертежи для строителей',
        'Концепция дизайна (эскизы)',
      ],
    },
    {
      title: 'Полный дизайн-проект',
      image: '/assets/images/services/full-project.png',
      serviceIncludes: [
        'Планировка',
        'Рабочие чертежи для строителей',
        'Концепция дизайна (эскизы)',
        '3D-визуализация',
        'Ведомость материалов',
      ],
    },
    {
      title: 'Управление и комплектация',
      image: '/assets/images/services/managment-and-complectation.png',
      serviceIncludes: [
        'Составление сметы',
        'Выбор подрядчиков и поставщиков',
        'Ведение графика работ и поставок',
        'Ведение ведомости платежей',
        'Скидки от поставщиков',
      ],
    },
    {
      title: 'Декорирование',
      image: '/assets/images/services/decorating.png',
      serviceIncludes: [
        'Обмер',
        'Расстановка мебели и оборудования',
        'Коллажи и эскизы',
        'Корректировка розеток, выключателей и света',
        'Подбор отделочных материалов',
      ],
    },
    {
      title: 'Авторский надзор',
      image: '/assets/images/services/authors-supervision.png',
      serviceIncludes: [
        'Проверка работ на соответствие проекту',
        'Консультирование строителей',
        'Консультирование смежных проектировщиков',
        'Консультирование заказчика',
        'Внесение изменений в чертежи',
      ],
    },
  ],
};
