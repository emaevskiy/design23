export default [
  {
    id: 0,
    type: 'question',
    question: 'Выберите тип помещения.',
    answersGroups: [
      {
        title: null,
        template: 'text-list',
        answers: [
          {
            value: 'Квартира',
            trigger: {
              quizType: 'apartaments',
            },
            isActive: false,
          },
          {
            value: 'Частный дом',
            trigger: {
              quizType: 'house',
            },
            isActive: false,
          },
        ],
        isActive: true,
      },
    ],
    ymGoal: 'enter',
    isCompleted: false,
  },
  {
    id: 1,
    type: 'question', // тип шага, может быть 'question' / 'result' / 'callback'
    question: 'Дизайн чего именно вам нужен?',
    // группировка ответов на вопросы в раздельные списки, свойство является обязателеным для шагов с типом question / result.
    answersGroups: [
      {
        title: 'Вся квартира',
        template: 'text-list', // тип шаблона, может быть 'text-list' / 'cards'
        answers: [
          {
            value: 'Студия',
            isActive: false,
          },
          {
            value: '1 комнатная',
            isActive: false,
          },
          {
            value: '2 комнатная',
            isActive: false,
          },
          {
            value: '3 комнатная',
            isActive: false,
          },
          {
            value: '4 комнатная и более',
            isActive: false,
          },
        ],
        isActive: true,
      },
      {
        title: 'Отдельные элементы',
        template: 'text-list',
        answers: [
          {
            value: 'Зал/Гостинная',
            isActive: false,
          },
          {
            value: 'Комната',
            isActive: false,
          },
          {
            value: 'Спальня',
            isActive: false,
          },
          {
            value: 'Кухня',
            isActive: false,
          },
          {
            value: 'Ванная',
            isActive: false,
          },
          {
            value: 'Детская комната',
            isActive: false,
          },
          {
            value: 'Другое',
            isActive: false,
          },
        ],
        isActive: true,
      },
    ],
    ymGoal: 'step1_1',
    isCompleted: false,
  },
  // {
  //   id: 2,
  //   type: 'question',
  //   question: 'Какая площадь вашего помещения?',
  //   answersGroups: [
  //     {
  //       title: null,
  //       template: 'text-field',
  //       fieldSettings: {
  //         type: 'number',
  //         min: 1,
  //         max: 200,
  //       },
  //       answer: {
  //         value: 20,
  //         isActive: false,
  //       },
  //       isActive: true,
  //     },
  //   ],
  //   ymGoal: 'step1_2',
  //   isCompleted: false,
  // },
  {
    id: 2,
    type: 'question',
    question: 'Какая площадь вашего помещения?',
    answersGroups: [
      {
        title: null,
        template: 'text-list',
        answers: [
          {
            value: 'до 40 кв.м',
            trigger: {
              objectSquare: 36,
            },
            isActive: false,
          },
          {
            value: 'от 40 до 100 кв.м',
            trigger: {
              objectSquare: 41,
            },
            isActive: false,
          },
          {
            value: 'свыше 100 кв.м',
            trigger: {
              objectSquare: 101,
            },
            isActive: false,
          },
        ],
        isActive: true,
      },
    ],
    ymGoal: 'step1_2',
    isCompleted: false,
  },
  {
    id: 3,
    type: 'question',
    question: 'В каком стиле хотите проект?',
    answersGroups: [
      {
        title: null,
        template: 'text-list',
        answers: [
          {
            // image: '/assets/images/designs/classic.png',
            value: 'Дизайн в классическом стиле',
            isActive: false,
          },
          {
            // image: '/assets/images/designs/loft.png',
            value: 'Дизайн в стиле лофт',
            isActive: false,
          },
          {
            // image: '/assets/images/designs/provence.png',
            value: 'Дизайн в стиле прованс',
            isActive: false,
          },
          {
            // image: '/assets/images/designs/high-tech.png',
            value: 'Дизайн в стиле хай-тек',
            isActive: false,
          },
          {
            // image: '/assets/images/designs/scandinavian.png',
            value: 'Дизайн в скандинавском стиле',
            isActive: false,
          },
          {
            // image: '/assets/images/designs/neoclassic.png',
            value: 'Дизайн в стиле неоклассика',
            isActive: false,
          },
          {
            // image: '/assets/images/designs/minimalistic.png',
            value: 'Дизайн в стиле минимализм',
            isActive: false,
          },
          {
            // image: '/assets/images/designs/others.png',
            value: 'Еще не определились',
            isActive: false,
          },
        ],
        isActive: true,
      },
    ],
    ymGoal: 'step1_3',
    isCompleted: false,
  },
  // {
  //   id: 4,
  //   type: 'result',
  //   question: 'Ориентировочная стоимость вашего проекта:',
  //   answersGroups: [
  //     {
  //       title: null,
  //       answers: [
  //         {
  //           value: 'Ждем вашего звонка, чтобы обсудить детали',
  //           trigger: {
  //             callRequest: true,
  //           }, // якорь для привязки какого-либо условия
  //           isActive: false,
  //         },
  //         {
  //           value: 'Хотим только чертежи и 3D-визуализацию реального проекта',
  //           trigger: {
  //             callRequest: false,
  //           },
  //           isActive: false,
  //         },
  //       ],
  //       isActive: true,
  //     },
  //   ],
  //   ymGoal: 'step1_4',
  //   isCompleted: false,
  // },
  {
    id: 4,
    type: 'result',
    question: 'Когда планируете приступить к реализации?',
    answersGroups: [
      {
        title: null,
        answers: [
          {
            value: 'В ближайшее время. Готовы обсудить детали',
            trigger: {
              callRequest: true,
            }, // якорь для привязки какого-либо условия
            isActive: false,
          },
          {
            value: 'Просто изучаем. Хотим посмотреть пример чертежей и 3D',
            trigger: {
              callRequest: false,
            },
            isActive: false,
          },
        ],
        isActive: true,
      },
    ],
    ymGoal: 'step1_4',
    isCompleted: false,
  },
  {
    id: 5,
    type: 'callback',
  },
];
