import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

// import stepsApartaments from '@/configs/quiz-apartaments.config';
// import stepsHouse from '@/configs/quiz-house.config';
import steps from '@/configs/quiz-common.config';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    quiz: {
      type: 'default',
      isStarted: false,
      isCompleted: false,
      results: null,
      minProjectPrice: 600,
      objectSquare: 0,
      steps,
    },
    form: {
      wasSend: false,
      isSending: false,
      requestType: 'none', // none/call/meet
    },
    projectPreviewPath: '/pdf/Талис 1.pdf',
  },
  getters: {
    quizSteps(state) {
      const quizSteps = [
        state.quiz.steps.entry,
        ...state.quiz.steps[state.quiz.type],
        state.quiz.steps.callback,
      ];

      return quizSteps;
    },
    requestType(state) {
      return state.form.requestType;
    },
    quizType(state) {
      return state.quiz.type;
    },
    projectPrice(state) {
      return state.quiz.objectSquare * state.quiz.minProjectPrice;
    },
  },
  mutations: {
    startQuiz(state) {
      state.quiz.isStarted = true;
    },
    setQuizType(state, payload) {
      state.quiz.type = payload;
    },
    setRequestType(state, payload) {
      state.form.requestType = payload;
    },
    setObjectSquare(state, payload) {
      state.quiz.objectSquare = payload;
    },
  },
  actions: {
    startQuiz({ commit }, data) {
      commit('startQuiz', data);
    },
    setQuizType({ commit }, data) {
      commit('setQuizType', data);
    },
    setRequestType({ commit }, data) {
      commit('setRequestType', data);
    },
    setObjectSquare({ commit }, data) {
      commit('setObjectSquare', data);
    },
    async fetchData({ commit }, { url, mutation }) {
      try {
        const response = await axios.get(url);
        commit(mutation, response.data);
      } catch (error) {
        /* eslint-disable no-console */
        console.log(error);
        console.log('Упс! Что-то пошло не так.');
      }
    },
  },
  modules: {
  },
});
