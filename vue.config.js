module.exports = {
  css: {
    loaderOptions: {
      sass: {
        prependData: `
          @import "@/scss/_normalize.scss";
          @import "@/scss/_fonts.scss";
          @import "@/scss/_mixins.scss";
          @import "@/scss/_vars.scss";
        `,
      },
    },
  },
};
