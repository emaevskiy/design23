<?
  require_once $_SERVER['DOCUMENT_ROOT'] . '/api/amocrm/src/Entity.php';
  require_once $_SERVER['DOCUMENT_ROOT'] . '/api/amocrm/src/Handler.php';
  require_once $_SERVER['DOCUMENT_ROOT'] . '/api/amocrm/src/Request.php';
  require_once $_SERVER['DOCUMENT_ROOT'] . '/api/amocrm/src/Lead.php';
  require_once $_SERVER['DOCUMENT_ROOT'] . '/api/amocrm/src/Contact.php';
  require_once $_SERVER['DOCUMENT_ROOT'] . '/api/amocrm/src/Note.php';
  require_once $_SERVER['DOCUMENT_ROOT'] . '/api/amocrm/src/Task.php';

  function calcTimeForCall($timeString = '00:00') 
  {
    $time = $timeString;

    if (!preg_match('/[0-2]?[0-9]:[0-6][0-9]/', $time)) {
      return null;
    }

    $hours = explode(':', $time)[0];
    $minutes = explode(':', $time)[1];

    $day = date('d');
    $month = date('m');
    $year = date('Y');

    $callTime = mktime($hours, $minutes, 00, $month, $day, $year);

    if (($callTime - time() - 3 * 3600) <= 0) {
      return $callTime + 24 * 3600;
    }
    return $callTime;
  }

  function createNewLead($leadName, $contactName, $contactPhone, $action, $timeForCall = null, $noteText = null) 
  {
    try {
      $api = new Handler('design23', 'info@design23.ru', true);
  
      [
        'LeadStatusId' => $leadStatusId,
        'ResponsibleUserId' => $responsibleUserId,
        'ContactFieldPhone' => $contactFieldPhone,
      ] = $api->config;
  
      /* Создаем новую сделку */
      $lead = new Lead();
      $lead->setName($leadName);
      $lead->setStatusId($leadStatusId);
      $lead->setResponsibleUserId($responsibleUserId);
      
      $api->request(new Request(Request::SET, $lead));

  
      /* Запоминаем ID созданной сделки */
      $lead = $api->last_insert_id;
  
      /* Создаем контакт и привязываем его к созданной ранее сделке */
      $contact = new Contact();
      $contact->setName($contactName);
      $contact->setLinkedLeadsId($lead);
      $contact->setCustomField($contactFieldPhone, $contactPhone, 'MOB');
  
      /* Проверяем существует ли уже контакт с таким номером телефона */
      $api->request(new Request(Request::GET, ['query' => $contactPhone], ['contacts', 'list']));
      $existingContact = $api->result ? $api->result->contacts[0] : false;
      
      /* Если контакт существует, то обновляем существующий */
      if ($existingContact) {
        $contact->setUpdate($existingContact->id, $existingContact->last_modified + 1);
        $contact->setResponsibleUserId($existingContact->responsible_user_id);
        $contact->setLinkedLeadsId($existingContact->linked_leads_id);
      }
      
      $api->request(new Request(Request::SET, $contact));


      /* Создаем задачу для ответственного менеджера */
      $task = new Task();
      $task->setElementId($lead);
      $task->setElementType(Task::TYPE_LEAD);
      $task->setTaskType(Task::CALL);
      $task->setResponsibleUserId($responsibleUserId);
      if ($timeForCall) {
        $task->setCompleteTill($timeForCall);
      }
  
      switch ($action) {
        case 'call':
          $task->setText('Клиент хочет получить консультацию.');
          break;
  
        case 'meet':
          $task->setText('Клиент хочет договориться о встрече.');
        break;
        
        default:
          $task->setText('Обработать заявку. (Связь с клиентом на ваше усмотрение).');
      }

      $api->request(new Request(Request::SET, $task));


      /* Создаем заметку с ответами на тест */
      if ($noteText) {
        $note = new Note();
        $note->setElementId($lead);
        $note->setElementType(Note::TYPE_LEAD);
        $note->setNoteType(Note::COMMON);
        $note->setText($noteText);

        $api->request(new Request(Request::SET, $note));
      }  
    } catch (\Exception $e) {
      header('Content-Type: text/html; charset=utf-8');
      echo $e->getMessage();
    }
  }
