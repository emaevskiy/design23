<?php

/**
 * This file makes clean URLs for your api requests
 * 
 * .htaccess config for make it alive:
 * RewriteEngine on
 * RewriteRule ^api/([0-9a-zA-Z]+)$ /api/api.php?endpoint=$1 [L] 
 */

$endpoint = trim($_GET['endpoint']);

// There are list of all your api endpoints and paths to them controllers
$endpointHandlers = array(
  'listen' => 'listen.php',
  'ping' => 'ping.php',
);

if (!$endpointHandlers[$endpoint]) {
  die('403 Access denied.');
}

require_once ($_SERVER['DOCUMENT_ROOT'] . '/api/' . $endpointHandlers[$endpoint]);
