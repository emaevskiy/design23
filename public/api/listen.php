<?php
  require_once $_SERVER['DOCUMENT_ROOT'] . '/api/amocrm.php';

  $post = json_decode(file_get_contents('php://input'));
  $post = $post->data;

  if (!isset($post->name) || !isset($post->phone)) {
    die ('Error: Name or phone is empty');
  }

  function get_ip()
  {
    if (getenv('HTTP_CLIENT_IP')) { $ip = $_SERVER['HTTP_CLIENT_IP']; }
    elseif (getenv('HTTP_X_FORWARDED_FOR')) { $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];	}
    else { $ip = $_SERVER['REMOTE_ADDR']; }
    return $ip;
  }

  function get_post_item($post, $key, $empty = null) 
  {
    $value = isset($post->$key) 
      ? $post->$key
      : $empty;
    $value = is_string($value) 
      ? htmlspecialchars(stripslashes(trim($value)))
      : $value;

    return $value;
  }

  $ip = get_ip();
  $name = get_post_item($post, 'name');
  $phone = get_post_item($post, 'phone');

  $timeForCall = get_post_item($post, 'timeForCall');
  $quizResults = get_post_item($post, 'quizResult');
  $responseAction = get_post_item($post, 'requestType');
  $leadType = get_post_item($post, 'quizType', 'unknown');
    
  $subject = 'New Lead';

  $message = '<div>Здравствуйте!</div><br>';
  $message.= '<div>Меня зовут <b>'.$name.'</b></div>';
  $message.= '<div>Мой телефон: <b>'.$phone.'</b></div>';
  if ($timeForCall) {
    $message.= '<div>Жду вашего звонка в <b>'.strtolower($timeForCall).'</b></div><br>';
  }

  if ($quizResults) {
    $message.= '<div>Мои результаты опроса:</div>';
    foreach($quizResults as $step) {
      $message.= '<div>'.$step->question.': <b>'.$step->answer.'</b></div>';
    };
  }
  $message.= '<br><div>IP: <b>'.$ip.'</b></div>';

  // $to  = "serge-82@mail.ru";
  // $to  = "jeydmc@gmail.com";
  $to  = "serge-82@mail.ru, jeydmc@gmail.com";

  $headers  = "Content-type: text/html; charset=UTF-8 \r\n";
  $headers .= "From: zayavka_s_saita@".$_SERVER['HTTP_HOST']; ;

  @mail($to, $subject, $message, $headers); 


  /* Форматируем данные для отправки в CRM */
  $phone = preg_replace('/[^\d+]/ui', '', $phone);

  switch (strtolower($leadType)) {
    case 'apartaments':
      $leadName = 'Квартира';
      break;
    
    case 'house':
      $leadName = 'Частный дом';
      break;
    
    default:
      $leadName = 'Вид помещения неизвестен';      
  }

  if ($timeForCall && strtolower($timeForCall) !== 'любое время') {
    $timeForCall = calcTimeForCall($timeForCall);
  } else {
    $timeForCall = null;
  }

  if ($quizResults) {
    $noteText = '';
    foreach($quizResults as $step) {
      $noteText.= $step->question.': '.$step->answer.PHP_EOL;
    };
  }

  createNewLead($leadName, $name, $phone, $responseAction, $timeForCall, $noteText);
  